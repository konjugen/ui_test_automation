package Tests;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;

import BaseClass.Base;
import Components.PaymentPage;

import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PaymentTest extends Base{

    private static boolean setUpIsDone = false;
    private PaymentPage _paymentPage;

    @Before
    public void Initiliaze() {
        if (setUpIsDone) {
            return;
        }
        setUpIsDone = true;
        super.Init();
        _paymentPage = new PaymentPage(driver);
        driver.get("https://meditopia.app/int/FuoEFN3QDgLo06lrOhySuWubL5IbbO4f/");
    }

    @Test
    public void fill_input_with_expected_parameters_return_success() {
            _paymentPage.FillCardHolderName().sendKeys("Ahmet Anıl Gürbüz");
            _paymentPage.FillCardNumber("arguments[0].value='4242 4444 4444 4443';");
            _paymentPage.SetCardExpirationMonth("05");
            _paymentPage.SetCardExpirationYear("2023");
            _paymentPage.FillCardCVV().sendKeys("333");
            _paymentPage.ClickPayment();
            Assert.assertEquals("CONGRATULATIONS!", _paymentPage.AssertionSuccess());
            setUpIsDone = false;
    }

    @Test
    public void fill_input_with_wrong_cardnumber_return_failed() {
            _paymentPage.FillCardHolderName().sendKeys("Ahmet Anıl Gürbüz");
            _paymentPage.FillCardNumber("arguments[0].value='4242 4444 4444 4440';");
            _paymentPage.SetCardExpirationMonth("05");
            _paymentPage.SetCardExpirationYear("2023");
            _paymentPage.FillCardCVV().sendKeys("333");
            _paymentPage.ClickPayment();
            Assert.assertEquals("", _paymentPage.AssertionFailed());
            setUpIsDone = false;
    }

    @After
    public void TearDown() {
        driver.quit();
    }
    
}