package Utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import BaseClass.Base;

public class Extensions implements IExtentions {

    @Override
    public void ClickEvet(By element) {
        Base.driver.findElement(element).click();
    }

    @Override
    public WebElement FindElementEvent(By element)
    {
        return Base.driver.findElement(element);
    }

    @Override
    public void SelectByValueEvent(By element, String value) {
        Select select = new Select(Base.driver.findElement(element));
        select.selectByValue(value);
    }

    @Override
    public void SubmitEvent(By element) {
        Base.driver.findElement(element).submit();
    }

    @Override
    public void FillByJs(By element, String argument) {
        WebElement wb = FindElementEvent(element);
        JavascriptExecutor jse = (JavascriptExecutor)Base.driver;
        jse.executeScript(argument, wb);
    }

    @Override
    public String Assert(By element) {
        String message = Base.driver.findElement(element).getText();
        return message;
    }
}