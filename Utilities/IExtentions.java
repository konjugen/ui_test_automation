package Utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public interface IExtentions {
    public void ClickEvet(By element);
    public WebElement FindElementEvent(By element);
    public void SelectByValueEvent(By element, String value);
    public void SubmitEvent(By element);
    public void FillByJs(By element, String argument);
    public String Assert(By element);
}