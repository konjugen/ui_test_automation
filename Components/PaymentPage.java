package Components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import BaseClass.Base;
import Utilities.Extensions;

public class PaymentPage extends Base{

    By cardHolderName = By.id("cardHolderName");
    By cardNumber = By.id("cardNumber");
    By cardExpirationMonth = By.id("cardExpirationMonth");
    By cardExpirationYear = By.id("cardExpirationYear");
    By cardCVV = By.id("cardCVV");
    By submitButton = By.xpath("//*[text()='START NOW          ']");
    By successPayment = By.xpath("//*[text()='CONGRATULATIONS!']"); 
    By errorCode = By.id("errorcodeid");
    
    Extensions extentions = new Extensions();

    public PaymentPage(WebDriver driver){
        Base.driver = driver;
        PageFactory.initElements(driver, this);

    } 

    public WebElement FillCardHolderName() {
        return extentions.FindElementEvent(cardHolderName);
    }

    public void FillCardNumber(String argument) {
         extentions.FillByJs(cardNumber, argument);
    }

    public void SetCardExpirationMonth(String value) {
        extentions.SelectByValueEvent(cardExpirationMonth, value);
    }

    public void SetCardExpirationYear(String value) {
        extentions.SelectByValueEvent(cardExpirationYear, value);
    }

    public WebElement FillCardCVV() {
        return extentions.FindElementEvent(cardCVV);
    }

    public void ClickPayment(){
        extentions.SubmitEvent(submitButton);
    }

    public String AssertionSuccess(){
        var message = extentions.Assert(successPayment);
        return message;
    }

    public String AssertionFailed(){
        var message = extentions.Assert(successPayment);
        return message;
    }
}